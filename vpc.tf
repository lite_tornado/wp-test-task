#Create VPC
resource "aws_vpc" "WP_VPC" {
    cidr_block = "10.0.0.0/16"
    tags = {
      name = "WP_VPC"
    }
}

#Create a public subnet 1
resource "aws_subnet" "PublicSubnet1" {
    vpc_id = aws_vpc.WP_VPC.id
    cidr_block = "10.0.1.0/24"
}

#Create a public subnet 2
resource "aws_subnet" "PublicSubnet2" {
    vpc_id = aws_vpc.WP_VPC.id
    cidr_block = "10.0.2.0/24"
}

#Create a private subnet 1
resource "aws_subnet" "PrivateSubnet1" {
    vpc_id = aws_vpc.WP_VPC.id
    cidr_block = "10.0.3.0/24"
}

#Create a private subnet 2
resource "aws_subnet" "PrivateSubnet2" {
    vpc_id = aws_vpc.WP_VPC.id
    cidr_block = "10.0.4.0/24"
}

#Create IGW
resource "aws_internet_gateway" "WP_IGW" {
    vpc_id = aws_vpc.WP_VPC.id
}

#Create route table for public subnets
resource "aws_route_table" "WP_PublicRT" {
    vpc_id = aws_vpc.WP_VPC.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.WP_IGW.id
    }
}

#Route table association public subnet1
resource "aws_route_table_association" "WP_PublicRTassociation1" {
    subnet_id = aws_subnet.PublicSubnet1.id
    route_table_id = aws_route_table.WP_PublicRT.id
}

#Route table association public subnet2
resource "aws_route_table_association" "WP_PublicRTassociation2" {
    subnet_id = aws_subnet.PublicSubnet2.id
    route_table_id = aws_route_table.WP_PublicRT.id
}