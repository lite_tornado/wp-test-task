#create ec2 instance

resource "aws_instance" "WP_EC2" {
  ami           = "ami-007855ac798b5175e" #Ubuntu AMI
  instance_type = "t3.micro"
  vpc_security_group_ids = [aws_security_group.WP_SG.id]
  subnet_id = aws_subnet.PrivateSubnet1.id
  associate_public_ip_address = true


  tags = {
    Name = "WP_EC2_instance"
  }
}

#create security group

resource "aws_security_group" "WP_SG" {
  name        = "WP_SG"
  description = "WP_SecurityGroup"
  vpc_id      = aws_vpc.WP_VPC.id

  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "WP_SG"
  }
}